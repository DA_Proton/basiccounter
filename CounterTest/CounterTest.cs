﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterLibrary;

namespace CounterTest
{
    [TestClass]
    public class CounterTest
    {
        [TestMethod]
        public void TestPlusUn()
        {
            CounterClass obj = new CounterClass();
            obj.plusUn();
            Assert.AreEqual(1, obj.getTotal());
        }
        [TestMethod]
        public void TestMoinsUn()
        {
            CounterClass obj = new CounterClass();
            obj.moinsUn();
            Assert.AreEqual(-1, obj.getTotal());
        }
        [TestMethod]
        public void TestRaz()
        {
            CounterClass obj = new CounterClass();
            Assert.AreEqual(0, obj.getTotal());
        }

    }
}
