TP noté de C# (prog windows 2019)

## Diagramme de classe
# ![](_UML/basicCounterUML.png)

## Réalisation
```
Manipulation des WinForms et C# via Visual Studio et .NET Framework
Application basique permettant de tenir un compte (pour des points de vie par exemple)
```
`Les valeures négatives ne sont pas permises`

## Author
> Quentin Champagne :) 