﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterLibrary
{
    public class CounterClass
    {
        protected int total;

        public CounterClass(int tot = 0)
        {
            this.total = tot;
        }

        public void plusUn()
        {
            this.total += 1;
        }
        public void moinsUn()
        {
            this.total -= 1;
        }
        public void raz()
        {
            this.total = 0;
        }
        public int getTotal()
        {
            return this.total;
        }
    }
}
